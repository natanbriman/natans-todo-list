import { IMAGES_PATH } from '../Utils/Constants.js';
import { formatDate } from '../Utils/Helpers.js';

export default class Todo {
  constructor(id, title, description, image, date = new Date(), isCompleted = false) {
    this._id = id;
    this._title = title;
    this._description = description;
    this._image = image;
    this._date = date;
    this._isCompleted = isCompleted;
  }

  get id() {
    return this._id;
  }
  get title() {
    return this._title;
  }
  get description() {
    return this._description;
  }
  get image() {
    return this._image;
  }
  get date() {
    return this._date;
  }
  get isCompleted() {
    return this._isCompleted;
  }

  toggleCompleted() {
    this._isCompleted = !this._isCompleted;
  }
  imagePath() {
    return `${IMAGES_PATH}/${this._image}`;
  }
  formattedDate() {
    return formatDate(this._date);
  }
}
