const todoElementTemplate = `<article class="todo-card" style="margin: 1em">
  <section class="two-thirds-width white">
    <div id="todo-header" class="flex-row max-width">
      <h1 class="todo-title"></h1>
    </div>

    <h4 class="todo-description"></h4>
  </section>

  <section class="third-width" style="display: flex; justify-content: end">
    <img style="height: 6em" class="todo-image" src="Images/DefaultImage.jpg" alt="Todo Image" />
  </section>
</article>`;

export default todoElementTemplate;
