import { todoList } from './TodoList.js';
import { updateTodosSection } from './TodoListSection.js';

export const currentTodoList = todoList.current;

document.addEventListener('DOMContentLoaded', () => {
  updateTodosSection(currentTodoList);
});
