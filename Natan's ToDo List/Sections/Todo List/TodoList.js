import Todo from '../../Models/Todo.js';
import { TODO_LIST_IN_LOCAL_STORAGE } from '../../Utils/Constants.js';
import {
  convertTodoObjectToClass,
  getItemFromLocalStorage,
  isTitleEqual,
  setItemToLocalStorage,
} from '../../Utils/Helpers.js';
import { updateTodosSection } from './TodoListSection.js';

const INITIAL_TODOS = [
  new Todo('ToDo 1', 'DESCRIPTION 1', 'Todo 1.jpg'),
  new Todo('ToDo 2', 'DESCRIPTION 2', 'Todo 2.jpg', new Date(2003, 1, 15)),
  new Todo('ToDo 3', 'DESCRIPTION 3', 'Todo 3.jpg', new Date()),
];

const getTodoListFromLocalStorage = () => {
  const todoListAsObjects = getItemFromLocalStorage(TODO_LIST_IN_LOCAL_STORAGE);

  if (!todoListAsObjects) {
    setItemToLocalStorage(TODO_LIST_IN_LOCAL_STORAGE, []);

    return [];
  }

  const todoListAsClass = todoListAsObjects.map(convertTodoObjectToClass);

  return todoListAsClass;
};

export const todoList = new Proxy(
  { current: getTodoListFromLocalStorage() },
  {
    set: (target, key, value) => {
      target[key] = value;
      updateTodosSection(value);
      setItemToLocalStorage(TODO_LIST_IN_LOCAL_STORAGE, value);

      return true;
    },
  }
);

export const setTodoList = (updatedTodos) => (todoList.current = updatedTodos);

export const addTodoToList = (newTodo) => {
  const updatedTodos = [...todoList.current, newTodo];

  setTodoList(updatedTodos);
};

export const deleteTodoFromList = (todoToDelete) => {
  const filteredTodos = todoList.current.filter((todo) => !isTitleEqual(todoToDelete, todo));
  setTodoList(filteredTodos);

  return filteredTodos;
};

// setItemToLocalStorage(TODO_LIST_IN_LOCAL_STORAGE, INITIAL_TODOS);
