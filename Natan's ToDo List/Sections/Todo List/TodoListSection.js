import todoElementTemplate from './Templates/Todo.js';
import {
  clearElement,
  countTodosStatus,
  getElementFromStringDocumentBySelector,
  getTodoStatusIconClassesList,
  getTodoChildElementsToUpdate,
  updateChildElementAttributeBySelector,
  updateElementText,
  isEqual,
  showElement,
  hideElement,
} from '../../Utils/Helpers.js';
import { selectedTodo, setSelectedTodo } from '../Selected Todo/SelectedTodo.js';
import { EMPTY_LIST_ELEMENT_ID, TODO_LIST_ELEMENT_ID } from '../../Utils/Constants.js';

export const todoListElement = document.getElementById('todo-list');

const updateTodoChildElements = (todoElement, todoInstance) => {
  const childElementsToUpdate = getTodoChildElementsToUpdate(todoInstance);

  childElementsToUpdate.map((childElementToUpdate) =>
    updateChildElementAttributeBySelector(todoElement, ...Object.values(childElementToUpdate))
  );
};

const createTodoStatusIcon = (todo) => {
  const todoStatusIcon = document.createElement('i');
  const todoStatusIconClassesList = getTodoStatusIconClassesList(todo.isCompleted);
  todoStatusIcon.classList.add('text-icon', 'double-font', ...todoStatusIconClassesList);

  return todoStatusIcon;
};

const setTodoToSelectedTodo = (todo) => {
  const currentSelectedTodo = selectedTodo.current;

  if (!isEqual(currentSelectedTodo, todo)) setSelectedTodo(todo);
};

const createTodoElement = (todo) => {
  const todoElement = getElementFromStringDocumentBySelector(todoElementTemplate, '.todo-card');
  todoElement.addEventListener('click', () => setTodoToSelectedTodo(todo));
  todoElement.id = todo.id;

  updateTodoChildElements(todoElement, todo);

  const todoHeaderElement = todoElement.querySelector('#todo-header');
  const todoStatusIcon = createTodoStatusIcon(todo);
  todoHeaderElement.append(todoStatusIcon);

  return todoElement;
};

const addTodoToList = (todo) => {
  const todoElement = createTodoElement(todo);
  const existingTodoElement = document.getElementById(todo.id);

  existingTodoElement
    ? todoListElement.replaceChild(todoElement, existingTodoElement)
    : todoListElement.append(todoElement);
};

const updateTodoListStatistics = (todoList) => {
  const completedTodosElement = document.getElementById('completed-todos');
  updateElementText(completedTodosElement, countTodosStatus(todoList));

  const openTodosElement = document.getElementById('open-todos');
  updateElementText(openTodosElement, countTodosStatus(todoList, false));
};

export const updateTodoList = (todoList) => {
  clearElement(todoListElement);

  todoList.map(addTodoToList);
};

const toggleTodoListVisibility = (todoList) => {
  const isTodoListEmpty = todoList.length === 0;
  const elementIdToShow = {
    true: EMPTY_LIST_ELEMENT_ID,
    false: TODO_LIST_ELEMENT_ID,
  };

  showElement(elementIdToShow[isTodoListEmpty]);
  hideElement(elementIdToShow[!isTodoListEmpty]);
};

export const updateTodosSection = (todoList) => {
  updateTodoList(todoList);
  updateTodoListStatistics(todoList);

  toggleTodoListVisibility(todoList);
};
