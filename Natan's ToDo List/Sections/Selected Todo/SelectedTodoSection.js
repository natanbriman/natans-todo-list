import {
  DELETE_TODO_BUTTON_ID,
  EMPTY_SELECTED_TODO_ELEMENT_ID,
  SELECTED_TODO_ELEMENT_ID,
  SELECTED_TODO_FOOTER_ELEMENT_ID,
} from '../../Utils/Constants.js';
import {
  appendIconToElementAsFirstChild,
  clearElement,
  createButtonElement,
  findTodoByTitle,
  getButtonIconClassesById,
  getToggleStatusButtonIconClasses,
  getToggleStatusButtonId,
  getToggleStatusButtonText,
  getUpdatedSelectedTodoElements,
  hideElement,
  isEmpty,
  replaceChildren,
  showElement,
  updateElementAttributeById,
  updateElementText,
} from '../../Utils/Helpers.js';
import { deleteTodoFromList, setTodoList, todoList } from '../Todo List/TodoList.js';
import { setSelectedTodo } from './SelectedTodo.js';

const updateToggleStatusButton = (status) => {
  const toggleButtonTextElement = document.createElement('div');
  const toggleButtonText = getToggleStatusButtonText(status);
  updateElementText(toggleButtonTextElement, toggleButtonText);

  const currentToggleStatusButtonId = getToggleStatusButtonId(status);
  const toggleTodoStatusButton = document.getElementById(currentToggleStatusButtonId);
  toggleTodoStatusButton.replaceChildren(toggleButtonTextElement);

  const toggleButtonIconClasses = getToggleStatusButtonIconClasses(status);
  appendIconToElementAsFirstChild(toggleButtonIconClasses, toggleTodoStatusButton);
};

const updateSelectedTodoElements = (newSelectedTodo) => {
  const updatedElements = getUpdatedSelectedTodoElements(newSelectedTodo);

  updatedElements.map((elementToUpdate) =>
    updateElementAttributeById(...Object.values(elementToUpdate))
  );
};

const createSelectedTodoButton = (onClick, buttonText, buttonId) => {
  const buttonElement = createButtonElement(onClick, buttonText, buttonId);
  const buttonIconClasses = getButtonIconClassesById(buttonId);

  appendIconToElementAsFirstChild(buttonIconClasses, buttonElement);
  buttonElement.classList.add('beautiful-button');

  return buttonElement;
};

const toggleTodoStatus = (todoToToggle) => {
  const todoInList = findTodoByTitle(todoList.current, todoToToggle);
  todoInList.toggleCompleted();

  return todoInList;
};

const updateSelectedTodoStatus = (selectedTodo) => {
  updateToggleStatusButton(selectedTodo.isCompleted);

  const updatedSelectedTodo = toggleTodoStatus(selectedTodo);
  setTodoList(todoList.current);
  setSelectedTodo(updatedSelectedTodo);
};

const createToggleStatusButton = (selectedTodo) => {
  const toggleStatusButtonText = getToggleStatusButtonText(selectedTodo.isCompleted);
  const toggleStatusButtonId = getToggleStatusButtonId(selectedTodo.isCompleted);

  const toggleStatusButton = createSelectedTodoButton(
    () => updateSelectedTodoStatus(selectedTodo),
    toggleStatusButtonText,
    toggleStatusButtonId
  );

  return toggleStatusButton;
};

const deleteSelectedTodoFromList = (todoToDelete) => {
  const filteredTodos = deleteTodoFromList(todoToDelete);
  const newSelectedTodo = filteredTodos[0] ?? {};

  setSelectedTodo(newSelectedTodo);
};

const createDeleteSelectedTodoButton = (selectedTodo) => {
  const deleteButtonText = 'Delete';
  const deleteTodoButton = createSelectedTodoButton(
    () => deleteSelectedTodoFromList(selectedTodo),
    deleteButtonText,
    DELETE_TODO_BUTTON_ID
  );

  return deleteTodoButton;
};

const updateSelectedTodoButtons = (newSelectedTodo) => {
  if (isEmpty(newSelectedTodo)) {
    const selectedTodoFooter = document.getElementById(SELECTED_TODO_FOOTER_ELEMENT_ID);

    clearElement(selectedTodoFooter);
  } else {
    const deleteTodoButton = createDeleteSelectedTodoButton(newSelectedTodo);
    const toggleStatusButton = createToggleStatusButton(newSelectedTodo);

    replaceChildren(SELECTED_TODO_FOOTER_ELEMENT_ID, [deleteTodoButton, toggleStatusButton]);
  }
};

const toggleSelectedTodoVisibility = (newSelectedTodo) => {
  const isSelectedTodoEmpty = isEmpty(newSelectedTodo);
  const elementIdToShow = {
    true: EMPTY_SELECTED_TODO_ELEMENT_ID,
    false: SELECTED_TODO_ELEMENT_ID,
  };

  showElement(elementIdToShow[isSelectedTodoEmpty]);
  hideElement(elementIdToShow[!isSelectedTodoEmpty]);
};

export const updateSelectedTodo = (newSelectedTodo) => {
  updateSelectedTodoElements(newSelectedTodo);
  updateSelectedTodoButtons(newSelectedTodo);

  toggleSelectedTodoVisibility(newSelectedTodo);
};
