import { SELECTED_TODO_IN_LOCAL_STORAGE } from '../../Utils/Constants.js';
import {
  convertTodoObjectToClass,
  getItemFromLocalStorage,
  isEmpty,
  setItemToLocalStorage,
} from '../../Utils/Helpers.js';
import { updateSelectedTodo } from './SelectedTodoSection.js';

export const getSelectedTodoFromLocalStorage = () => {
  const selectedTodoAsObject = getItemFromLocalStorage(SELECTED_TODO_IN_LOCAL_STORAGE);

  if (isEmpty(selectedTodoAsObject)) {
    setItemToLocalStorage(SELECTED_TODO_IN_LOCAL_STORAGE, {});

    return {};
  }

  const selectedTodoAsClass = convertTodoObjectToClass(selectedTodoAsObject);

  return selectedTodoAsClass;
};

export const selectedTodo = new Proxy(
  { current: getSelectedTodoFromLocalStorage() },
  {
    set: (target, key, value) => {
      const newSelectedTodo = value;
      target[key] = newSelectedTodo;

      updateSelectedTodo(newSelectedTodo);
      setItemToLocalStorage(SELECTED_TODO_IN_LOCAL_STORAGE, newSelectedTodo);

      return true;
    },
  }
);

export const setSelectedTodo = (todo) => (selectedTodo.current = todo);
