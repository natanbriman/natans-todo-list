import { getSelectedTodoFromLocalStorage } from './SelectedTodo.js';
import { updateSelectedTodo } from './SelectedTodoSection.js';

export const currentSelectedTodo = getSelectedTodoFromLocalStorage();

document.addEventListener('DOMContentLoaded', () => {
  updateSelectedTodo(currentSelectedTodo);
});

// setItemToLocalStorage(todos.current[0]);
