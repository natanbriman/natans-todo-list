import Todo from '../../Models/Todo.js';
import { DEFAULT_IMAGE_PATH } from '../../Utils/Constants.js';
import { createURLFromFile, removeChildElement } from '../../Utils/Helpers.js';

const titleElement = document.getElementById('add-todo-title');
const descriptionElement = document.getElementById('add-todo-description');
export const imageInputElement = document.getElementById('add-todo-image-input');

export const createTodoFromInputElements = () => {
  const title = titleElement.value;
  const description = descriptionElement?.value;
  const imagePath = imageInputElement?.files[0]?.name || imageInputElement.defaultValue;

  const todo = new Todo(Math.random().toString(), title, description, imagePath);

  return todo;
};

const clearValueInputs = () => {
  titleElement.value = '';
  descriptionElement.value = '';
  imageInputElement.value = '';
};

const setImageElementToDefaultImage = () => {
  const imageElement = document.getElementById('add-todo-image');

  imageElement.src = DEFAULT_IMAGE_PATH;
};

export const clearAddTodoSection = () => {
  clearValueInputs();
  setImageElementToDefaultImage();
  removeChildElement('add-todo-image-section', 'clear-image-button');
};

export const createImageFromInput = () => {
  const inputImageFile = imageInputElement.files[0];
  const imageElement = document.getElementById('add-todo-image');

  imageElement.src = createURLFromFile(inputImageFile);
};

export const clearImageAndHideButton = () => {
  setImageElementToDefaultImage();

  removeChildElement('add-todo-image-section', 'clear-image-button');
};
