import { DELETE_TODO_BUTTON_ICON_CLASSES } from '../../Utils/Constants.js';
import {
  addChildElement,
  appendIconToElementAsFirstChild,
  createButtonElement,
} from '../../Utils/Helpers.js';
import {
  clearAddTodoSection,
  clearImageAndHideButton,
  createImageFromInput,
  createTodoFromInputElements,
  imageInputElement,
} from './AddTodoSection.js';

export const addTodoFormElement = document.getElementById('add-todo-form');

imageInputElement.addEventListener('change', () => {
  createImageFromInput();

  const existingClearImageButton = document.getElementById('clear-image-button');
  if (existingClearImageButton) return;

  const clearImageButton = createButtonElement(
    clearImageAndHideButton,
    'Clear',
    'clear-image-button'
  );
  clearImageButton.classList.add('beautiful-button');
  appendIconToElementAsFirstChild(DELETE_TODO_BUTTON_ICON_CLASSES, clearImageButton);

  addChildElement('add-todo-image-section', clearImageButton);
});

addTodoFormElement.addEventListener('submit', async (event) => {
  event.preventDefault();

  const newTodo = createTodoFromInputElements();
  const { addTodoToList } = await import('../Todo List/TodoList.js');

  addTodoToList(newTodo);
  clearAddTodoSection();
});
