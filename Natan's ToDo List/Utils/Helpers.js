import Todo from '../Models/Todo.js';
import {
  BUTTON_ICON_CLASSES,
  COMPLETED_TODO_ICON_CLASSES,
  COMPLETED_TODO_STATUS_BUTTON_ICON_CLASSES,
  COMPLETED_TODO_STATUS_BUTTON_TEXT,
  COMPLETE_TODO_BUTTON_ID,
  OPEN_TODO_ICON_CLASSES,
  OPEN_TODO_STATUS_BUTTON_ICON_CLASSES,
  OPEN_TODO_STATUS_BUTTON_TEXT,
  RESTORE_TODO_BUTTON_ID,
} from './Constants.js';

export const createURLFromFile = (file) => window.URL.createObjectURL(file);

export const removeChildElement = (parentId, childId) => {
  const parentElement = document.getElementById(parentId);
  const childElement = document.getElementById(childId);

  if (childElement) parentElement.removeChild(childElement);
};

export const addChildElement = (parentId, childElement) => {
  const parentElement = document.getElementById(parentId);

  parentElement.append(childElement);
};

export const updateChildElementAttributeBySelector = (
  parentElement,
  selector,
  attribute,
  value
) => {
  const childElement = parentElement.querySelector(selector);

  childElement[attribute] = value;
};

export const convertStringToHTML = (string) => new DOMParser().parseFromString(string, 'text/html');

export const getElementFromStringDocumentBySelector = (documentAsString, selector) => {
  const documentFromFile = convertStringToHTML(documentAsString);
  const element = documentFromFile.querySelector(selector);

  return element;
};

export const getTodoStatusIconClassesList = (todoStatus) =>
  todoStatus ? COMPLETED_TODO_ICON_CLASSES : OPEN_TODO_ICON_CLASSES;

export const countTodosStatus = (todos, isCompleted = true) =>
  todos.filter((todo) => todo.isCompleted === isCompleted).length;

export const clearElement = (element) => {
  element.replaceChildren([]);

  element.style = '';
};

export const isTitleEqual = (firstTodo, secondTodo) => secondTodo.title === firstTodo.title;

export const createButtonElement = (onClick, buttonText, buttonId) => {
  const buttonElement = document.createElement('button');
  buttonElement.addEventListener('click', onClick);
  buttonElement.id = buttonId;

  const textElement = document.createElement('div');
  updateElementText(textElement, buttonText);
  buttonElement.append(textElement);

  return buttonElement;
};

export const appendIconToElementAsFirstChild = (iconClasses, element) => {
  const iconElement = document.createElement('i');
  iconElement.classList.add(...iconClasses);

  element.insertBefore(iconElement, element.children[0]);
};

export const getToggleStatusButtonText = (isCompleted) =>
  isCompleted ? COMPLETED_TODO_STATUS_BUTTON_TEXT : OPEN_TODO_STATUS_BUTTON_TEXT;

export const getToggleStatusButtonIconClasses = (isCompleted) =>
  isCompleted ? COMPLETED_TODO_STATUS_BUTTON_ICON_CLASSES : OPEN_TODO_STATUS_BUTTON_ICON_CLASSES;

export const findTodoByTitle = (todos, todoToFind) =>
  todos.find((todoInList) => isTitleEqual(todoToFind, todoInList));

export const updateElementText = (element, text) => {
  element.textContent = text;
};

export const updateElementAttributeById = (elementId, attribute, value) => {
  const element = document.getElementById(elementId);

  element[attribute] = value;
};

export const getUpdatedSelectedTodoElements = (newSelectedTodo) => {
  const isClearSelectedTodo = isEmpty(newSelectedTodo);

  return [
    {
      id: 'selected-todo',
      attribute: 'style',
      updatedValue: isClearSelectedTodo
        ? ''
        : `background-image: url('${newSelectedTodo.imagePath()}')`,
    },
    {
      id: 'selected-todo-title',
      attribute: 'textContent',
      updatedValue: isClearSelectedTodo ? '' : newSelectedTodo.title,
    },
    {
      id: 'selected-todo-date',
      attribute: 'textContent',
      updatedValue: isClearSelectedTodo ? '' : newSelectedTodo.formattedDate(),
    },
    {
      id: 'selected-todo-description',
      attribute: 'textContent',
      updatedValue: isClearSelectedTodo ? '' : newSelectedTodo.description,
    },
  ];
};

export const getButtonIconClassesById = (buttonIdToFind) =>
  BUTTON_ICON_CLASSES.find(({ buttonId }) => buttonId === buttonIdToFind).iconClasses;

export const getToggleStatusButtonId = (status) =>
  status ? RESTORE_TODO_BUTTON_ID : COMPLETE_TODO_BUTTON_ID;

export const getItemFromLocalStorage = (itemName) => {
  const itemAsString = localStorage.getItem(itemName);

  return JSON.parse(itemAsString);
};

export const setItemToLocalStorage = (itemName, value) => {
  const valueAsString = JSON.stringify(value);

  localStorage.setItem(itemName, valueAsString);
};

export const convertTodoObjectToClass = (todoObject) => {
  const { _id, _title, _description, _image, _date, _isCompleted } = todoObject;
  const todoAsClass = new Todo(_id, _title, _description, _image, new Date(_date), _isCompleted);

  return todoAsClass;
};

export const replaceChildren = (elementId, newChildren) => {
  const element = document.getElementById(elementId);

  element.replaceChildren(...newChildren);
};

export const getTodoChildElementsToUpdate = (todo) => [
  {
    selector: '.todo-title',
    attribute: 'textContent',
    updatedValue: todo.title,
  },
  {
    selector: '.todo-description',
    attribute: 'textContent',
    updatedValue: todo.description,
  },
  {
    selector: '.todo-image',
    attribute: 'src',
    updatedValue: todo.imagePath(),
  },
];

export const isEqual = (firstObject, secondObject) =>
  Object.entries(firstObject).toString() === Object.entries(secondObject).toString();

export const isEmpty = (object) => object === null || !Object.entries(object).length;

export const formatDate = (date) => date.toLocaleDateString();

export const hideElement = (elementId) => {
  const element = document.getElementById(elementId);

  element.style.display = 'none';
};

export const showElement = (elementId) => {
  const element = document.getElementById(elementId);

  element.style.display = 'inline';
};
