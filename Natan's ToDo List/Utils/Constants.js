export const IMAGES_PATH = 'Images';
export const DEFAULT_IMAGE_NAME = 'DefaultImage.jpg';
export const DEFAULT_IMAGE_PATH = `${IMAGES_PATH}/${DEFAULT_IMAGE_NAME}`;
export const COMPLETED_TODO_ICON_CLASSES = ['fa', 'fa-check', 'green'];
export const OPEN_TODO_ICON_CLASSES = ['fa', 'fa-close', 'red'];
export const COMPLETED_TODO_STATUS_BUTTON_TEXT = 'Restore';
export const OPEN_TODO_STATUS_BUTTON_TEXT = 'Complete';
export const COMPLETE_TODO_BUTTON_ID = 'complete-todo-button';
export const RESTORE_TODO_BUTTON_ID = 'restore-todo-button';
export const DELETE_TODO_BUTTON_ID = 'delete-todo-button';
export const COMPLETED_TODO_STATUS_BUTTON_ICON_CLASSES = ['fa', 'fa-refresh', 'text-icon'];
export const OPEN_TODO_STATUS_BUTTON_ICON_CLASSES = ['fa', 'fa-check', 'text-icon'];
export const DELETE_TODO_BUTTON_ICON_CLASSES = ['fa', 'fa-trash-o', 'text-icon'];
export const BUTTON_ICON_CLASSES = [
  { buttonId: DELETE_TODO_BUTTON_ID, iconClasses: DELETE_TODO_BUTTON_ICON_CLASSES },
  { buttonId: COMPLETE_TODO_BUTTON_ID, iconClasses: OPEN_TODO_STATUS_BUTTON_ICON_CLASSES },
  { buttonId: RESTORE_TODO_BUTTON_ID, iconClasses: COMPLETED_TODO_STATUS_BUTTON_ICON_CLASSES },
];
export const TODO_LIST_IN_LOCAL_STORAGE = 'TODO_LIST';
export const SELECTED_TODO_IN_LOCAL_STORAGE = 'SELECTED_TODO';
export const SELECTED_TODO_FOOTER_ELEMENT_ID = 'selected-todo-footer';
export const TODO_LIST_ELEMENT_ID = 'todo-list';
export const EMPTY_LIST_ELEMENT_ID = 'empty-list';
export const SELECTED_TODO_ELEMENT_ID = 'selected-todo';
export const EMPTY_SELECTED_TODO_ELEMENT_ID = 'empty-selected-todo';
